# ohos-slidr

Another openharmony slider / seekbar, but different :-)


# Download

```java
dependencies {
    implementation 'io.openharmony.tpc.thirdlib:ohos-slidr:1.0.2'
}
```

[![png](medias/slidr1.png)](https://gitee.com/openharmony-tpc/ohos-slidr)

```xml
<com.github.florent37.harmonyslidr.Slidr
        ohos:width="250dp"
        ohos:height="wrap_content"
        app:slidr_regions_centerText="false"
        app:slidr_region_leftColor="#4CAF50" />
```

# Step

[![png](medias/slidr2_1.png)](https://gitee.com/openharmony-tpc/ohos-slidr)
[![png](medias/slidr2_2.png)](https://gitee.com/openharmony-tpc/ohos-slidr)

```xml
<com.github.florent37.harmonyslidr.Slidr
       ohos:id="$+id:slideure"
       ohos:width="250dp"
       ohos:height="wrap_content"
       app:slidr_region_leftColor="#4CAF50"
       app:slidr_step_colorizeAfterLast="true" />
```

```java
final Slidr slidr = (Slidr) findComponentById(ResourceTable.Id_slideure);
slidr.setMax(500);
slidr.addStep(new Slidr.Step("test", 1500, new Color(0xFF007E90), new Color(0xFF111111)));
slidr.setTextMax("max\nvalue");
slidr.setCurrentValue(300);
slidr.setListener(new Slidr.Listener() {
    @Override
    public void valueChanged(Slidr slidr, float currentValue) {

    }

    @Override
    public void bubbleClicked(Slidr slidr) {

    }
});
```

# Region

[![png](medias/slidr_region.png)](https://gitee.com/openharmony-tpc/ohos-slidr)

```xml
<com.github.florent37.harmonyslidr.Slidr
        ohos:id="$+id:slideure_regions"
       ohos:layout_margin="20dp"
       ohos:layout_width="250dp"
       ohos:layout_height="wrap_content"
       app:slidr_regions="true"
       app:slidr_region_leftColor="@color/colorLeft"
       app:slidr_region_rightColor="@color/colorRight"
       app:slidr_regions_textFollowRegionColor="true"
       app:slidr_regions_centerText="true"
       app:slidr_draw_bubble="false"
       />
```

```java
final Slidr slidr = (Slidr) findViewById(R.id.slideure_regions);
slidr.setMax(3000);
slidr.setRegionTextFormatter(new Slidr.RegionTextFormatter() {
    @Override
    public String format(int region, float value) {
        return String.format("region %d : %d", region, (int) value);
    }
});
slidr.addStep(new Slidr.Step("test", 1500, Color.parseColor("#007E90"), Color.parseColor("#111111")));
```



License
--------

    Copyright 2017 Florent37, Inc.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
